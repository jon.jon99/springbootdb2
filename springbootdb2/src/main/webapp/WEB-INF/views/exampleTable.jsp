<!doctype html>
<html lang="en">
<%@ include file="/WEB-INF/layouts/include.jsp" %>
<%@ include file="/WEB-INF/layouts/head.jsp" %>
<body id="demo-body">
	<div id="demo-main-div" class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				<h1>Example Page</h1>
				<%@ include file="/WEB-INF/layouts/message.jsp" %>
				<table class="table table-hover table-striped table-bordered">
					<thead>
						<tr>
							<th>ID</th>
							<th>First Name</th>
							<th>Last Name</th>
						</tr>
					</thead>
					<tbody>
						<c:if test="${empty exampleList}">
							<tr>
								<td colspan="3">
									No data found
								</td>
							</tr>
						</c:if>
						<c:if test="${not empty exampleList}">
							<c:forEach var="item" items="${exampleList}">
								<tr>
									<td>${item.id}</td>
									<td>${item.firstName}</td>
									<td>${item.lastName}</td>
								</tr> 
							</c:forEach>
						</c:if>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>