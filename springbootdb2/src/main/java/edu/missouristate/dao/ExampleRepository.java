package edu.missouristate.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import edu.missouristate.model.Example;
import edu.missouristate.util.MSU;

@Repository
public class ExampleRepository {
	
	@Autowired
	JdbcTemplate template;
	
    public ExampleRepository() {
    }    
    
	public List<Example> getExamples() {
		String sql = "SELECT * " + 
	                 "  FROM examples ";
		
		Object[] args = null;		
		List<Example> exampleList = template.query(sql, args, MSU.EXAMPLE_BPRM);		
		return exampleList;
	}
	
	public List<Example> getExamplesOLD() {
		String sql = "SELECT id, first_name, last_name " + 
	                 "  FROM examples ";
		Object[] args = null;
		List<Map<String, Object>> result = template.queryForList(sql, args);
		List<Example> exampleList = new ArrayList<Example>();
		
		for (Map<String, Object> map : result) {
			Example example = new Example();
			
			for (Map.Entry<String, Object> entry : map.entrySet()) {
				String key = entry.getKey();
				String data = ((entry.getValue() == null) ? null : entry.getValue().toString());

				switch(key) {
				case "ID":					
					example.setId(Integer.valueOf(data));
					break;
				case "FIRST_NAME":
					example.setFirstName(data);
					break;
				case "LAST_NAME":
					example.setLastName(data);
					break;
				}
			}
			
			if (example.getId() != null) {
				exampleList.add(example);
			}
		}
		
		return exampleList;
	}
    
}

